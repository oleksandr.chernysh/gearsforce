$(window).load(function () {

    var JSAPP = JSAPP || {};
    var body = $('body');

    JSAPP.fullPage = function () {
        $('#fullpage').fullpage({
            //Navigation
            menu: '#menu',
            anchors: ['main', 'ourTeam', 'technologies', 'developmentProject', 'developmentTeam', 'developmentLab', 'reinforce', 'contacts'],
            navigation: true,
            navigationPosition: 'right',
            resize: false,

            //Design
            verticalCentered: true,
            responsiveWidth: 992,

            onLeave: function (index, newIndex, direction) {
                var rocket = $('.rocket');
                var currentSection = $('.section').eq(newIndex - 1);
                var currentPlatform = currentSection.find('.r-platform');

                rocket.removeClass('moveUp moveDown');
                setTimeout(function () {
                    (direction == 'down') ? rocket.addClass('moveDown') : rocket.addClass('moveUp');
                }, 10);

                function animatePlatform() {
                    currentSection.find('.re-an').removeClass('animated');
                    setTimeout(function() {
                        currentSection.find('.re-an').addClass('animated');
                    }, 10);
                }

                // Rocket animation
                if ((newIndex == 4) || (newIndex == 5) || (newIndex == 6)) {
                    animatePlatform();
                    $(window).resize(function () {
                        animatePlatform();
                    });
                }
                // Rocket animation
            },
            afterLoad: function (anchorLink, index) {
                //Timer on slide 3
                if (index == 2) {
                    $('.counter').each(function () {
                        var elem = $(this);
                        var num = parseInt(elem.html());
                        var currentNum = 0;
                        var i = (elem.parent().hasClass('experience')) ? 5 : 1;

                        function loop() {
                            setTimeout(function () {
                                elem.html(currentNum);
                                currentNum = currentNum + i;
                                if (currentNum <= num) {
                                    loop();
                                }
                            }, 25);
                        }
                        loop();
                    });
                }
                //Timer on slide 3 end

                // Rocket position
                if ((index == 4) || (index == 5) || (index == 6)) {
                    var currentSection = $('.section').eq(index - 1);
                    var rocket = $('.rocket');
                    var currentPlatform = currentSection.find('.r-platform');
                    function setRocketPosition() {
                        rocket.css({
                            top: currentPlatform.offset().top + 21,
                            left: currentPlatform.offset().left + 172,
                            display: 'block'
                        });
                    }

                    setTimeout(function() {
                        setRocketPosition();
                    }, 100);


                    $(window).resize(function () {
                        setTimeout(function() {
                            setRocketPosition();
                        }, 100);
                    });
                }
                // Rocket position end
            }
        });
    };

    JSAPP.menu = function () {
        $('#menu-trigger').click(function () {
            body.toggleClass('menu-opened');
            return false;
        });
        $('#menu a').click(function () {
            body.removeClass('menu-opened');
        });
        $('#logo').click(function () {
            $.fn.fullpage.moveTo('main');
            return false;
        });
        $('.btn-project').click(function () {
            $.fn.fullpage.moveTo('developmentProject');
            return false;
        });
        $('.btn-team').click(function () {
            $.fn.fullpage.moveTo('developmentTeam');
            return false;
        });
        $('.btn-lab').click(function () {
            $.fn.fullpage.moveTo('developmentLab');
            return false;
        });
    };

    JSAPP.fullPage();
    JSAPP.menu();

}); //window load